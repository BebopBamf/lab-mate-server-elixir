defmodule LabMateServer.Repo do
  use Ecto.Repo,
    otp_app: :lab_mate_server,
    adapter: Ecto.Adapters.Postgres
end
